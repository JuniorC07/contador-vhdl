--******************************************************************************
--                                                                             *
--                  Copyright (C) 2009 Altera Corporation                      *
--                                                                             *
-- ALTERA, ARRIA, CYCLONE, HARDCOPY, MAX, MEGACORE, NIOS, QUARTUS & STRATIX    *
-- are Reg. U.S. Pat. & Tm. Off. and Altera marks in and outside the U.S.      *
--                                                                             *
-- All information provided herein is provided on an "as is" basis,            *
-- without warranty of any kind.                                               *
--                                                                             *
-- Module Name: adder                        File Name: adder.vhd              *
--                                                                             *
-- Module Function: This file implements a 16-bit adder                        *
--                                                                             *
-- REVISION HISTORY:                                                           *
--  Revision 1.0    11/24/2009 - Initial Revision  for QII 9.1                 *
--******************************************************************************

-- Insert library and use clauses
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Begin entity declaration for "adder"
ENTITY adder IS
	-- Begin port declaration
	PORT (rst   		   : IN BIT; --Reset
			ck    		   : IN BIT; -- pulso de clock
			switch		   : IN BIT; -- interruptor para trocar de soma para subtração e vice-versa
			ckCounterOut   : OUT INTEGER RANGE 50 DOWNTO 0; -- Saída que representa a contagem de pulsos de clock(máximo 50 pulsos)
			mainCounterOut : OUT INTEGER RANGE 15 DOWNTO 7); -- Saída que representa o contador(tamanho definido pela regra de negócio do trabalho)
	
-- End entity 
END adder;

--   Begin architecture 
ARCHITECTURE teste OF adder IS
BEGIN
		abc: PROCESS(ck, rst, switch)
			VARIABLE ckCounter :INTEGER RANGE 50 DOWNTO 0; -- Váriavel auxiliar utilizada para a contagem de clocks dentro do processo
			VARIABLE counter :INTEGER RANGE 15 DOWNTO 7; -- Váriavel auxiliar manipulada para definir a saída do contador
		BEGIN
			IF (rst ='1')    THEN -- estado de reset 
				ckCounter := 0; -- contador de pulsos é zerado
				counter := 7; -- contador principal com seu valor mínimo(para facilitar a visualização)
			ELSIF (ck'EVENT and ck = '1') THEN -- Toda vez que o pulso estiver subindo, soma + 1 no countador de pulsos
					ckCounter := ckCounter + 1;
					IF (switch = '0' and counter < 15) THEN -- Quando o pulso for 0 e o contador for < 15, será somado 1 no contador principal
							counter := counter + 1;
					ELSIF (switch = '1' and counter > 7) THEN -- Quando o pulso for 1 e o contador > 7, será subtraido 1 no contador principal
							counter := counter - 1;
					END IF;
			END IF;
		ckCounterOut <= ckCounter; -- Atribuição da váriavel da quantidade de pulsos de clock
		mainCounterOut <= counter; -- Atribuição da váriavel do contador
		END PROCESS abc;

-- End architecture
END ARCHITECTURE teste;

